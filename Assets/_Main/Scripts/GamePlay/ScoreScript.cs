﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ScoreScript : MonoBehaviour
{
    internal int scoreValue;
    TextMeshProUGUI scoreTm;
    private void Start()
    {
        scoreTm = transform.Find("Score").GetComponentInChildren<TextMeshProUGUI>();
    }
    //Increases the score as a counter.
    internal IEnumerator IncreaseScore(int increasedValue)
    {
        for(int i=0; i <= increasedValue; i++)
        {
            scoreValue += 1;
            scoreTm.text = scoreValue.ToString();

            yield return new WaitForSeconds(0.01f);
        }
    }

}
