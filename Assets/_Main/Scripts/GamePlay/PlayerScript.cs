﻿using UnityEngine;
using System.Collections;
using Hsynyavz;
using MoreMountains.NiceVibrations;

public class PlayerScript : MonoBehaviour
{
    Vector3 firstLocalScale;

    GameManager gameManager;
    ScoreScript scoreScript;

    public GameObject effect;
    readonly float speed = 3;
    readonly float radius = 5.0F;
    readonly float power = 12.0F;

    bool isContactPipe = false;

    void Start()
    {
        firstLocalScale = transform.localScale;
        gameManager = FindObjectOfType<GameManager>();
        scoreScript = FindObjectOfType<ScoreScript>();
    }

    void Update()
    {
        if (gameManager.isGameStart)
        {
            ConfigureScale();
            transform.Translate(Vector3.up * Time.deltaTime * speed);
        }
        if (gameManager.isGameFinish)
        {
            transform.Translate(Vector3.up * Time.deltaTime * speed * 2);
        }
    }

    void ConfigureScale()
    {
        if (Input.GetMouseButton(0))
        {
            if (isContactPipe == false)
            {
                transform.localScale = Vector3.Slerp(transform.localScale, Vector3.zero, Time.deltaTime * 2);
            }
        }
        else
        {
            transform.localScale = Vector3.Slerp(transform.localScale, firstLocalScale, Time.deltaTime * 4);
        }
    }


    private void OnTriggerStay(Collider other)
    {
        TriggerControl(other);
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pipe"))
        {
            isContactPipe = false;
        }
    }

    void TriggerControl(Collider other)
    {
        switch (other.tag)
        {
            case "Pipe":
                isContactPipe = true;
                break;
            case "Corn":
                var temp = transform.Find("InstantiatePosition").transform;
                var clone = Instantiate(effect, temp.transform.position, Quaternion.Euler(new Vector3(270, 180, 180)));
                StartCoroutine(scoreScript.IncreaseScore(1));
                HapticManager.Haptic((Hsynyavz.HapticType)HapticTypes.LightImpact);
                Destroy(other.gameObject);
                break;
            case "Obstacle":
                gameManager.FinishWithFailure();
                HapticManager.Haptic((Hsynyavz.HapticType)HapticTypes.HeavyImpact);
                ForceExplosion();
                break;
            case "Finish":
                gameManager.FinishSuccessfully();
                break;
            default:
                break;
        }
    }

    void ForceExplosion()
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null && hit.CompareTag("Player"))
            {
                hit.GetComponent<MeshCollider>().enabled = false;
                hit.GetComponent<SphereCollider>().enabled = true;
                
                rb.isKinematic = false;
                rb.AddExplosionForce(power * 10, explosionPos, radius, 3.0F);
            }
        }
    }

    
}
