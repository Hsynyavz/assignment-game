﻿using UnityEngine;
using Cinemachine;
public class GameManager : MonoBehaviour
{
    UIManager UIManager;

    internal bool isGameStart = false;
    internal bool isGameFinish = false;

    private void Start()
    {
        UIManager = FindObjectOfType<UIManager>();
    }

    public void FinishSuccessfully()
    {
        ConfigureCinemachine();

        isGameStart = false;
        isGameFinish = true;
        UIManager.successPanel.SetActive(true);
    }

    public void FinishWithFailure()
    {
        ConfigureCinemachine();

        isGameStart = false;

        UIManager.failPanel.SetActive(true);
    }

    //Cinemachine Follow and LookAt adjustment
    void ConfigureCinemachine()
    {
        var cinemachine = GameObject.Find("CM vcam1").GetComponent<CinemachineVirtualCamera>();
        cinemachine.Follow = null;
        cinemachine.LookAt = GameObject.FindGameObjectWithTag("Player").transform;
    }




}




