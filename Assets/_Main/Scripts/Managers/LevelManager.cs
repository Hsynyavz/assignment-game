﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    int level;
    void Start()
    {
        Debug.Log("Level Sayısı: " + SceneManager.sceneCountInBuildSettings);
        level = PlayerPrefs.GetInt("Level") % SceneManager.sceneCountInBuildSettings - 1;
        if (level <= 0)
        {
            level = 1;
        }

        Debug.Log("Level : " + level);

        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        yield return new WaitForSeconds(0.2f);
        Debug.Log("Level Yükleme : " + level);

        SceneManager.LoadScene(level);

    }



}
