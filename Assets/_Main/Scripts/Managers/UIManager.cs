﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class UIManager : Singleton<UIManager>
{
    Button tapToStartBtn;
    Button tapToRetryBtn;
    Button tapToContinue;

    public GameObject successPanel;
    public GameObject failPanel;

    TextMeshProUGUI levelText;


    // Start is called before the first frame update
    void Start()
    {
        levelText = transform.Find("LevelBar").GetComponentInChildren<TextMeshProUGUI>();
        tapToStartBtn = transform.Find("FullscreenButton").GetComponent<Button>();
        failPanel = transform.Find("FullscreenFail").gameObject;
        tapToRetryBtn = failPanel.GetComponentInChildren<Button>();
        successPanel = transform.Find("FullscreenSuccess").gameObject;
        tapToContinue = successPanel.GetComponentInChildren<Button>();

        levelText.SetText("LEVEL " + PlayerPrefs.GetInt("Level", 1).ToString());

        tapToContinue.onClick.AddListener(TapToContinue);
        tapToRetryBtn.onClick.AddListener(TapToRetry);
        tapToStartBtn.onClick.AddListener(TapToStart);

        successPanel.SetActive(false);
        failPanel.SetActive(false);
        tapToStartBtn.gameObject.SetActive(true);
    }


    private void TapToContinue()
    {
        PlayerPrefs.SetInt("Level", PlayerPrefs.GetInt("Level") + 1);
        var level = SceneManager.GetActiveScene().buildIndex + 1;
        level = level % (SceneManager.sceneCountInBuildSettings);
        if (level == 0)
        {
            level = 1;
        }

        SceneManager.LoadScene(level);
    }

    private void TapToRetry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    
    private void TapToStart()
    {
        tapToStartBtn.gameObject.SetActive(false);
        FindObjectOfType<GameManager>().isGameStart = true;
    }

   
}
