﻿namespace Hsynyavz
{
    public enum HapticType
    {
        Selection,
        Success,
        Warning,
        Failure,
        LightImpact,
        MediumImpact,
        HeavyImpact,
        RigidImpact,
        SoftImpact,
        None
    }
}